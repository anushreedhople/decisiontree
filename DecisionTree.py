import math

class Tree:
    def __init__(self):
        self.lines = []
        self.class_labels = []
        self.training_data = {}
        self.left = None
        self.right = None
        self.split_point = 0.0
        self.attribute = 0
        self.label = None

class DecisionTree:
    def __init__(self):
        self.class_labels = []
        self.training_data = {}  #key is the id of the attribute and the value is an array of attribute-values
        self.test_data = []

    def append_class_label(self, label):
        self.class_labels.append(label)

    def append_training_data(self, key, value):
        if key not in self.training_data:
            self.training_data[key] = [value]
        else:
            values = self.training_data[key]
            values.append(value)
            self.training_data[key] = values

    def append_test_data(self, key, value):
        if key not in self.test_data:
            self.test_data[key] = [value]
        else:
            values = self.test_data[key]
            values.append(value)
            self.test_data[key] = values

    def compute(self, tree):
        min_gain = None
        min_mid_point = 0.0
        min_attribute_id = 0
        # Get mid points between sorted unique pairs
        for key,values in tree.training_data.items():
            # data - all values for single attribute. Get unique values in the data array
            unique_data = []
            [unique_data.append(x) for x in values if x not in unique_data]
            # Sort unique data points
            unique_data.sort()
            # Iterate through sorted unique data and get mid points
            mid_points = []
            for p in range(0, len(unique_data)-1):
                m = (float(unique_data[p]) + float(unique_data[p+1]))/2
                mid_points.append(m)

            # Use mid points to get x and y for Information gain of attribute at all mid points
            for mid_point in mid_points:
                info_gain = self.compute_info_gain(values, mid_point, tree.class_labels)
                if min_gain is None:
                    min_gain = info_gain
                    min_mid_point = mid_point
                    min_attribute_id = key
                elif float(info_gain) < float(min_gain):
                    min_gain = info_gain
                    min_mid_point = mid_point
                    min_attribute_id = key
                elif float(info_gain) == float(min_gain):
                    if key < min_attribute_id:
                        min_mid_point = mid_point
                        min_attribute_id = key
        split_info = (min_mid_point,min_attribute_id)
        return split_info


    def compute_info_gain(self, values, mid_point, class_labels):
        unique_class_labels = []
        [unique_class_labels.append(x) for x in class_labels if x not in unique_class_labels]
        less_than = 0
        more_than = 0
        sum = len(values)
        less_than_class_label = {}
        more_than_class_label = {}
        for label in unique_class_labels:
            less_than_class_label[label] = 0
            more_than_class_label[label] = 0
        for i in range(0,len(values)):
            key = class_labels[i]
            if float(values[i]) <= float(mid_point):
                less_than += 1
                less_than_class_label[key] = less_than_class_label[key] + 1
            else:
                more_than += 1
                more_than_class_label[key] = more_than_class_label[key] + 1
        #There can be more than 2 class labels.
        less_than_info_gain = float(less_than)/sum * self.compute_ixy(less_than_class_label)
        more_than_info_gain = float(more_than)/sum * self.compute_ixy(more_than_class_label)
        info_gain =  less_than_info_gain + more_than_info_gain
        return info_gain

    def compute_ixy(self, labels):
        values = labels.values()
        total = sum(values)
        ratio = 0.0
        for key,value in labels.items():
            if value == 0:
                continue
            else:
                key_ratio = float(value)/total
                ixy = key_ratio * math.log(key_ratio,2)
                ratio = float(ratio) - ixy
        return ratio


    def compute_labels_and_training_data(self, tree):
        for line in tree.lines:
            data = line.rstrip().split(' ')
            tree.class_labels.append(data[0])
            values = []
            for a in range(1, len(data)):
                # attribute[0] will give the id of the attribute
                attribute = data[a].rstrip().split(':')
                key = attribute[0]
                value = attribute[1]
                if key not in tree.training_data:
                    tree.training_data[key] = [value]
                else:
                    values = tree.training_data[key]
                    values.append(value)
                    tree.training_data[key] = values

    def most_frequent(self, List):
        return max(set(List), key=List.count)


    def split_tree(self, root):
        root.left = Tree()
        root.right = Tree()
        for line in root.lines:
            data = line.rstrip().split(' ')
            for a in range(1, len(data)):
                attribute = data[a].rstrip().split(':')
                if attribute[0] == root.attribute and float(attribute[1]) <= float(root.split_point):
                    root.left.lines.append(line.rstrip())
                elif attribute[0] == root.attribute and float(attribute[1]) > float(root.split_point):
                    root.right.lines.append(line.rstrip())
        return root


if __name__ == '__main__':

    tree = DecisionTree()
    root = Tree()

    # Accept input and store locally
    file = open("input24.txt", "r")
    lines = file.readlines()

    # Read file line by line and store test data
    for line in lines:
        data = line.rstrip().split(' ')
        if int(data[0]) != -1:
            root.lines.append(line.rstrip())
        else:
            tree.test_data.append(line.rstrip())

    tree.compute_labels_and_training_data(root)


    # Compute level 1 of tree
    split_info = tree.compute(root)
    root.split_point = split_info[0]
    root.attribute = split_info[1]

    # Split the lines into left and right tree
    root = tree.split_tree(root)

    tree.compute_labels_and_training_data(root.left)
    tree.compute_labels_and_training_data(root.right)

    # Check if all nodes of left tree have same class then no need to compute split point again. Then assign the class label
    result = False
    result = len(root.left.class_labels) > 0 and all(element == root.left.class_labels[0] for element in root.left.class_labels)
    if result:
        root.left.label = root.left.class_labels[0]
    else:
        split_info = tree.compute(root.left)
        root.left.split_point = split_info[0]
        root.left.attribute = split_info[1]
        # Compute root.left.left.label and root.left.right.label
        root.left = tree.split_tree(root.left)
        tree.compute_labels_and_training_data(root.left.left)
        tree.compute_labels_and_training_data(root.left.right)
        # Assign the most common class label
        root.left.left.label = tree.most_frequent(root.left.left.class_labels)
        root.left.right.label = tree.most_frequent(root.left.right.class_labels)

    # Same for right node
    result = False
    result = len(root.right.class_labels) > 0 and all(element == root.right.class_labels[0] for element in root.right.class_labels)
    if result:
        root.right.label = root.right.class_labels[0]
    else:
        split_info = tree.compute(root.right)
        root.right.split_point = split_info[0]
        root.right.attribute = split_info[1]
        # Compute root.right.left.label and root.right.right.label
        root.right = tree.split_tree(root.right)
        tree.compute_labels_and_training_data(root.right.left)
        tree.compute_labels_and_training_data(root.right.right)
        # Assign the most common class label
        root.right.left.label = tree.most_frequent(root.right.left.class_labels)
        root.right.right.label = tree.most_frequent(root.right.right.class_labels)



    #Apply decision tree on test data
    for line in tree.test_data:
        label = None
        data = line.rstrip().split(' ')
        attributes = {}
        for a in range(1, len(data)):
            attributes_data = data[a].rstrip().split(':')
            attributes[attributes_data[0]] = attributes_data[1]
        if float(attributes[root.attribute]) <= float(root.split_point):
            if root.left.label != None:
                print root.left.label
            elif float(attributes[root.left.attribute]) <= float(root.left.split_point):
                print root.left.left.label
            else:
                print root.left.right.label
        else:
            if root.right.label != None:
                print root.right.label
            elif float(attributes[root.right.attribute]) <= float(root.right.split_point):
                print root.right.left.label
            else:
                print root.right.right.label





