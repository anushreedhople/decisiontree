This algorithm builds a decision tree classifier for multi-class classification with continuous feature/attribute values. 
The decision tree uses binary split and a threshold to split data. 

If attribute X <=  threshold ; then left node; else right node.
where the best X and threshold  are for the algorithm to determine. 
The algorithm uses Information Gain to contruct the tree.
The same can also be implemented with Gain Ratio or Split Ratio

When training a DT, if splitting on either attribute X1 or X2  gives us the best information gain, the algorithm chooses the smaller of X1 and X2.
In prediction, if both labels L1 and L2 have the same number of training instances at a DT leaf node , the algorithm predicts the smaller of L1 and L2 .

Test cases are added for validation.